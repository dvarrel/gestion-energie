<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="PROJET-GESTION-ENERGIE" ID="ID_262039811" CREATED="1475170365491" MODIFIED="1475560287500" LINK="http://r.ebay.com/bZvqCx"><hook NAME="MapStyle" zoom="0.75">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="commandes" POSITION="right" ID="ID_1493977494" CREATED="1475170365492" MODIFIED="1475170593460">
<node TEXT="pilotage fil pilote" ID="ID_1994399405" CREATED="1475170365492" MODIFIED="1475208208779">
<node TEXT="TLP663J ou MOC3041M" ID="ID_1099526960" CREATED="1475170365492" MODIFIED="1475578356778" LINK="http://fr.rs-online.com/web/p/optocoupleurs/7603198/">
<hook URI="img/ordres_fil_pilote.png" SIZE="0.46686074" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="relayage 230V~" ID="ID_1768358278" CREATED="1475170365494" MODIFIED="1475170598915">
<node TEXT="circulateurs" ID="ID_173395348" CREATED="1475170365494" MODIFIED="1475170598916"/>
<node TEXT="vanne 3-4 voies" ID="ID_1182849031" CREATED="1475170365493" MODIFIED="1475170598913"/>
<node TEXT="pcf8574" ID="ID_1739394675" CREATED="1475208353031" MODIFIED="1475577620341">
<hook URI="img/pcf8574.jpg" SIZE="0.5069618" NAME="ExternalObject"/>
<node TEXT="" ID="ID_1164354526" CREATED="1475554895344" MODIFIED="1475554895344">
<node TEXT="" ID="ID_785919218" CREATED="1475208359903" MODIFIED="1475211398365">
<hook URI="img/relais-230.jpg" SIZE="0.38008428" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="" ID="ID_1481921051" CREATED="1475211405180" MODIFIED="1475211473214">
<hook URI="img/relais-statiques.jpg" SIZE="0.4175752" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="traitement" POSITION="right" ID="ID_1819903777" CREATED="1475170365494" MODIFIED="1475170593462">
<node TEXT="Wemos D1" ID="ID_544164578" CREATED="1475211525890" MODIFIED="1475560715847" LINK="https://fr.aliexpress.com/item/WEMOS-D1-mini-Pro-16M-bytes-external-antenna-connector-ESP8266-WIFI-Internet-of-Things-development-board/32724692514.html">
<hook URI="img/wemos-pro.jpg" SIZE="0.44524023" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="mesures" POSITION="left" ID="ID_460089775" CREATED="1475170365495" MODIFIED="1475181157949">
<node TEXT="temp&#xe9;rature" ID="ID_1682142642" CREATED="1475170365495" MODIFIED="1475170598924">
<node TEXT="DS18B20 : -55+125&#xb0;C" ID="ID_1368509324" CREATED="1475170365495" MODIFIED="1475560300355">
<hook URI="img/ds18b20.png" SIZE="0.36197093" NAME="ExternalObject"/>
</node>
<node TEXT="MAX31855 (i2c)" ID="ID_1492264434" CREATED="1475559457826" MODIFIED="1475559526436">
<hook URI="img/max31855.jpg" SIZE="0.29978386" NAME="ExternalObject"/>
</node>
<node TEXT="sonde K (-200+800&#xb0;C)" ID="ID_92607312" CREATED="1475559478924" MODIFIED="1475559548366">
<hook URI="img/thermocoupleK.png" SIZE="0.6835846" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="donn&#xe9;es compteur EDF" ID="ID_945010057" CREATED="1475170365496" MODIFIED="1475210014903">
<node TEXT="SFH620A-1" ID="ID_1716664669" CREATED="1475170365496" MODIFIED="1475185195213" LINK="http://www.conrad.fr/ce/fr/product/154005/Optocoupleur-SFH620A-1-DIP-4-ACbidirectional-Vishay/SHOP_AREA_14738">
<hook URI="img/compteur-teleinfo.png" SIZE="0.2537553" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="humidit&#xe9;" ID="ID_986008910" CREATED="1475182075177" MODIFIED="1475208131851">
<node TEXT="HTU21D" ID="ID_1851385742" CREATED="1475556446646" MODIFIED="1475556476307">
<hook URI="img/htu21D.jpg" SIZE="0.34015894" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="intensit&#xe9;" ID="ID_808010176" CREATED="1475182022689" MODIFIED="1475558080096">
<hook URI="img/acs712.png" SIZE="0.5799494" NAME="ExternalObject"/>
<node TEXT="ADS1115" ID="ID_1543231334" CREATED="1477058783421" MODIFIED="1477058799389">
<hook URI="img/ads1115.jpg" SIZE="0.5818075" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="supervision" POSITION="right" ID="ID_352002780" CREATED="1475170365496" MODIFIED="1475170593463">
<node TEXT="programmation chauffage" ID="ID_392539530" CREATED="1475170365494" MODIFIED="1475170598917"/>
<node TEXT="alerte sms (api free)" ID="ID_1136220516" CREATED="1475170365494" MODIFIED="1475209562644"/>
<node TEXT="diagnostic" ID="ID_1532267216" CREATED="1475170365497" MODIFIED="1475578470171"/>
<node TEXT="historique" ID="ID_632616761" CREATED="1475578492066" MODIFIED="1475578500876"/>
<node TEXT="pilotage &#xe0; distance Web&#xa;1.pilotage&#xa;2. mise &#xe0; jour&#xa;3. pr&#xe9;-configuration" ID="ID_1227444667" CREATED="1475578530028" MODIFIED="1475578532540"/>
</node>
</node>
</map>
