#include "stdlib_noniso.h"
#include "HardwareSerial.h"
#include "ArduinoJson/Json/JsonSerializer.hpp"
#include <sys/_stdint.h>
#include "def.h"

SSD1306Wire display(SSD1306_ADDRESS, PIN_SDA, PIN_SCL);

void gestion_energie::wifi_init() {
  Serial.println("wifi_init()");
  display.drawString(0, 14, "start WIFI");
  display.display();
  Serial.setDebugOutput(true);
  //WiFi.persistent(false);//important pour le stockage en flash
  Serial.printf("\nSSID : %s \npsk : %s\n", WiFi.SSID().c_str(), WiFi.psk().c_str());
  String AP = WiFi.macAddress();
  AP.replace(":", "");
  AP.toUpperCase();
  AP.remove(0, 6);
  AP = "ESP_" + AP;
  WiFiManager wifiManager;
  wifiManager.autoConnect(AP.c_str());

  Serial.printf("\nMAC : %s\n", WiFi.macAddress().c_str());
  Serial.printf("IP : %s/%s\n", String(WiFi.localIP().toString()).c_str(), String(WiFi.subnetMask().toString()).c_str());
  Serial.printf("GW : %s\n", String(WiFi.gatewayIP().toString()).c_str());
  Serial.printf("SSID %s -> %d dB\n", WiFi.SSID().c_str(), WiFi.RSSI());

  MAC = WiFi.macAddress();
  MAC.toUpperCase();

  Serial.print("Udp server started at port ");
  Serial.println(UDP_LOCAL_PORT);
  Udp.begin(UDP_LOCAL_PORT);
  broadcast = WiFi.localIP();
  broadcast[3] = 255;
}

void gestion_energie::HTTPmustReset() {
  doc.clear();
  doc["mac"] = MAC;
  doc["query"] = "must_reset";
  String data = "";
  serializeJson(doc, data);
  HTTPpost(URLPOST, data);
  deserializeJson(reponse, data);
  if (reponse["must_reset"] == 1) {
    Serial.println("reset requested by user !");
    //if call ESP.restart();  GPIO0(D3) need to be HIGH and GPIO15(D8) = LOW
    Serial.printf("D3:%u D8:%u\n", digitalRead(D3), digitalRead(D8));
    ESP.restart();
  }
}

void gestion_energie::gestion_init() {
  Serial.println("gestion_init()");
  
  this->nbPCF8574=0;
  for (uint8_t i = 0; i < NBMAXPCF8574; i++) {
    if(!this->pcf8574[i].begin(PCF_FIRST_ADDRESS+i)) break;
    this->nbPCF8574++;
    this->pcf8574[i].write8(0xFF);  //switch off all outputs;
  }
  this->nbADS1015=0;
  for (uint8_t i = 0; i < NBMAXADS1015; i++) {
    if (!this->ads1015[i].begin(ADS_FIRST_ADDRESS + i)) break;
      this->nbADS1015++;
      this->ads1015[i].setDataRate(RATE_ADS1015_3300SPS);
  }
  
  #if TELEINFO
  tinfo.init();
  tinfo.attachNewFrame(teleinfoJSON);
  #endif

  doc.clear();
  this->I2Cscanner();
  this->ds18search();
  doc["query"] = "scan";
  doc["mac"] = MAC;
  String data = "";
  serializeJson(doc, data);
  serializeJson(doc, Serial);
  Serial.println();
  this->HTTPpost(URLPOST, data);

  //initialize ds18 and acs712 sensors
  //request ds18,acs712 and pcf8574 parameters
  doc.clear();
  data = "";
  doc["query"] = "param";
  doc["mac"] = MAC;
  serializeJson(doc, data);
  Serial.println(data);
  this->HTTPpost(URLPOST, data);
  Serial.println(data);
  DeserializationError err = deserializeJson(reponse, data);
  if (err) Serial.println(err.f_str());
  if (reponse.size() != 0) {
    nbDS18 = reponse["ds18"].size();
    if (nbDS18>NBMAXDS18) nbDS18=NBMAXDS18;
    Serial.printf("database has %u ds18b20\n", this->nbDS18);
    for (uint8_t i = 0; i < this->nbDS18; i++) {
      String address = reponse["ds18"][i];
      uint8_t x = 0;
      for (uint8_t j = 0; j < 16; j = j + 2) {
        String s = address.substring(j, j + 2);
        uint8_t number = (uint8_t)strtol(&s[0], NULL, 16);
        this->sensorDS18[i].id[x] = number;
        x++;
      }
    }

    this->nbACS712 = reponse["acs712"].size();
    Serial.printf("database has %u acs712\n", this->nbACS712);
    if (nbACS712>NBMAXACS712) nbACS712=NBMAXACS712;
    for (uint8_t i = 0; i < this->nbACS712; i++) {
      uint8_t s = reponse["acs712"][i].as<uint8_t>();
      switch (s) {
        case 5:
          this->acs712[i].sensitivity = ACS712_SENS5A;
          break;
        case 20:
          this->acs712[i].sensitivity = ACS712_SENS20A;
          break;
        case 30:
          this->acs712[i].sensitivity = ACS712_SENS30A;
          break;
      }
    }
  }
  Serial.printf("ds18 addresses\n");
  for (uint8_t p = 0; p < this->nbDS18; p++) {
    for (uint8_t q = 0; q < 7; q++) Serial.printf("%02X-", this->sensorDS18[p].id[q]);
    Serial.printf("%02X\n", this->sensorDS18[p].id[7]);
  }
  Serial.printf("pcf8574 addresses: ");
  for (uint8_t i = 0; i < this->nbPCF8574; i++) {
    Serial.printf("0x%02X ", pcf8574[i].getAddress());
  }
  Serial.printf("\nads1015 addresses: ");
  for (uint8_t i = 0; i < this->nbADS1015; i++) {
    Serial.printf("0x%02X ", ADS_FIRST_ADDRESS+i);
  }
  Serial.printf("\nacs712 sensitivity: ");
  for (uint8_t i = 0; i < this->nbACS712; i++) {
    Serial.printf("%u ", acs712[i].sensitivity);
  }
  Serial.println();
  Serial.println(date);
}

void gestion_energie::HTTPgetsetOutputs() {
  this->last_outputs = this->outputs;

  doc.clear();
  doc["mac"] = MAC;
  doc["query"] = "get_outputs";  //request for new outputs
  String data = "";
  serializeJson(doc, data);
  this->HTTPpost(URLPOST, data);
  deserializeJson(reponse, data);
  uint8_t force = 0;
  if (reponse.size() != 0) {
    JsonVariant variant1 = reponse["get_outputs"];
    if (variant1.is<uint32_t>()) this->outputs = variant1;
    JsonVariant variant2 = reponse["force"];
    if (variant2.is<uint8_t>()) force = variant2;
  }
  uint32_t n = this->outputs;
  char bin[33]="00000000000000000000000000000000";
  for (uint8_t i = 0; i < 32; i++) {
    if (n & 0x80000000) bin[i]='1';
    n <<= 1;
  }
  Serial.printf("outputs: %s \n", bin);
  this->relays(outputs);
  if (force == 1 && last_outputs != outputs) {
    doc["mac"] = MAC;
    doc["query"] = "set_outputs";
    doc["outputs"] = outputs;  //maj outputs on force
    String data = "";
    serializeJson(doc, data);
    this->HTTPpost(URLPOST, data);
    Serial.println(data);
    Serial.println();
  }
}

void gestion_energie::HTTPsendvalues() {
  //StaticJsonDocument<2048> mesures; soft reset on esp !!!
  DynamicJsonDocument mesures(2048);
  mesures["outputs"] = outputs;
  JsonArray temp = mesures.createNestedArray("temp");
  char buf[6];  
  for (uint8_t i = 0; i < nbDS18; i++) {
    dtostrf(sensorDS18[i].temp, -5, 1, buf);
    temp.add(buf); //précision 1/10
  }
  /*for (uint8_t i = 0; i < nbDS18; i++) {
    String key = "t" + String(i);
    mesures[key] = sensorDS18[i].temp;
  }*/
  JsonArray irms = mesures.createNestedArray("irms");
  for (uint8_t i = 0; i < nbACS712; i++) {
    dtostrf(acs712[i].Irms, -5, 2, buf);
    irms.add(buf);    
  }
  /*for (uint8_t i = 0; i < nbACS712; i++) {
    String key = "i" + String(i);
    mesures[key] = acs712[i].Irms;
  }*/
  doc.clear();
  doc["mac"] = MAC;
  doc["query"] = "data";
  doc["data"] = mesures;
  #if TELEINFO
  doc["ti"] = teleinfoData;
  #endif
  String data = "";
  serializeJson(doc, data);
  serializeJson(doc, Serial);
  Serial.println();
  HTTPpost(URLPOST, data);
  deserializeJson(reponse, data);
  this->date = reponse["date"].as<String>();
}

void gestion_energie::HTTPpost(const char* url, String& data) {
  std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
  client->setInsecure();
  HTTPClient https;
  //Serial.println(url);
  https.begin(*client, url);
  https.addHeader("Content-Type", "application/json");
  int httpCode = https.POST(data);  // httpCode <0 on error
  if (httpCode > 0) {
    //Serial.printf("HTTP code: %d\n", httpCode);
    if (httpCode == HTTP_CODE_OK) {
      data = https.getString();
      Serial.printf("[HTTPS] POST reply: %s\n", data.c_str());
    }
  } else {
    Serial.printf("[HTTPS] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
  }
  https.end();
}

void gestion_energie::ds18read() {
  Serial.printf("### mesuring %d ds18 ###\n",this->nbDS18);
  uint8_t data[12];
  ds18.reset();
  ds18.skip();
  ds18.write(0x44, 0);  // start conversion, with parasite power on at the end
  delay(800);           // maybe 750ms is enough, maybe not
  for (uint8_t x = 0; x < nbDS18; x++) {
    ds18.reset();
    ds18.select(sensorDS18[x].id);
    ds18.write(0xBE);  // Read Scratchpad
    for (uint8_t i = 0; i < 9; i++) data[i] = ds18.read();
    // we need 9 bytes : B1-B0=temp, B4=conf and B8=CRC
    if (OneWire::crc8(data, 8) != data[8]) {
      Serial.printf("T%d=CRC error\t", x);
      sensorDS18[x].temp = 127;
    } else {
      int16_t raw = (data[1] << 8) | data[0];
      //Serial.printf("sizeof raw %d %d\t",sizeof(raw),raw);
      uint8_t cfg = (data[4] & 0x60);
      // at lower res, the low bits are undefined, so let's zero them
      if (cfg == 0x00) raw = raw & ~7;       // 9 bit resolution, 93.75 ms
      else if (cfg == 0x20) raw = raw & ~3;  // 10 bit res, 187.5 ms
      else if (cfg == 0x40) raw = raw & ~1;  // 11 bit res, 375 ms
      //// default is 12 bit resolution, 750 ms conversion time      
      sensorDS18[x].temp = (float)raw / 16.0;
      Serial.printf("T%d=%.2f C ", x, sensorDS18[x].temp);
    }
  }
  Serial.println();
}

void gestion_energie::ireads() {
  Serial.printf("### mesuring %d acs712 ###\n",this->nbACS712);
  uint8_t i = 0;
  for (uint8_t j=0; j<nbADS1015;j++){
    for (uint8_t ANx = 0; ANx < 4; ANx++) {
      //Serial.printf("i%u j%u AN%u\n",i,j,ANx);
      ads1015[j].startADCReading(ANx, true);
      int16_t Nmax = 0, Nmin = 2047, Nread;
      uint32_t dt = millis(); //50Hz T=20ms
      while (millis()-dt < 21) {
        Nread = ads1015[j].getLastConversionResults();
        Nmax = _max(Nmax, Nread);
        Nmin = _min(Nmin, Nread);
      }
      ads1015[j].readADC_SingleEnded(ANx);//stop continous ADC
      if (Nmin < 0) Nmin = 0;
      float Umax = ads1015[j].computeVolts(Nmax);
      float Umin = ads1015[j].computeVolts(Nmin);
      if (acs712[i].sensitivity != 0)
        acs712[i].Irms = (Umax - Umin) / (0.002 * acs712[i].sensitivity * sqrt(2));
      else acs712[i].Irms = 0;
      float Peff = acs712[i].Urms * acs712[i].Irms;
      Serial.printf("Irms%d\tNmin=%d\tNmax=%d\tUmin=%.3f\tUmax=%.3f\tIeff=%.3f\tPeff=%.3f\n", i, Nmin, Nmax, Umin, Umax, acs712[i].Irms, Peff);
      i++;
      if(i==nbACS712-1) break;
    }
  }
}


void gestion_energie::HTTPupdate() {
  String HTTP_X_ESP8266_VERSION = "ESP_" + MAC;
  Serial.printf("HttpUpdate : %s\n", HTTP_X_ESP8266_VERSION.c_str());
  t_httpUpdate_return ret = ESPhttpUpdate.update(client, URLUPDATE, HTTP_X_ESP8266_VERSION);
  switch (ret) {
    case HTTP_UPDATE_FAILED:
      Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
      break;
    case HTTP_UPDATE_NO_UPDATES:
      Serial.println("HTTP_UPDATE_NO_UPDATES");
      break;
    case HTTP_UPDATE_OK:
      Serial.println("HTTP_UPDATE_OK");
      break;
  }
}

#if TELEINFO
void gestion_energie::teleinfo(uint16_t delayms) {
  uint32_t start = millis();
  myserial.begin(1200);
  delay(1);
  myserial.flush();
  while (millis() < (start + delayms)) {
    if (myserial.available())
      tinfo.process(myserial.read());
    delay(1);
    yield();
  }
  myserial.end();
  * /
}

void gestion_energie::teleinfoJSON(ValueList* me) {
  doc["values"] = tinfo.valuesDump();
  if (me) {
    while (me->next) {
      me = me->next;
      if (me->value && strlen(me->value)) {
        boolean isNumber = true;
        uint8_t c;
        char* p = me->value;
        while (*p && isNumber) {
          if (*p < '0' || *p > '9')
            isNumber = false;
          p++;
        }
        if (!isNumber) doc[String(me->name)] = String(me->value);
        else doc[String(me->name)] = atol(me->value);
      }
    }
  }
  tinfo.listDelete();
  doc["buffersize"] = doc.size();
  teleinfoData = "";
  serializeJson(doc, teleinfoData);
  Serial.println(teleinfoData);
}
#endif

void gestion_energie::relays(uint32_t nums) {
  for (uint8_t i = 0; i < nbPCF8574; i++) {
    pcf8574[i].write8(0xFF - (uint8_t)nums);
    nums >>= 8;
    uint8_t error = pcf8574[i].lastError();
    if (error != 0) Serial.printf("pcf8574[%d] (I2C) error: %d\n", i, error);
  }
  //i2c error  0:success 1:data too long to fit in transmit buffer
  // 2:received NACK on transmit of address
  // 3:received NACK on transmit of data
  // 4:other error
}


void gestion_energie::UDPsendRequest() {
  doc.clear();
  doc["mac"] = WiFi.macAddress();
  Udp.beginPacket(broadcast, UDP_REMOTE_PORT);
  serializeJson(doc, Udp);
  Udp.println();
  Udp.endPacket();
}

void gestion_energie::UDPsendValues() {
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.printf("Received packet of size %d/%d\n", packetSize, UDP_TX_PACKET_MAX_SIZE);
    Serial.printf("From %s\n", String(Udp.remoteIP().toString()).c_str());
    char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);

    //StaticJsonDocument<UDP_TX_PACKET_MAX_SIZE> incoming;
    DynamicJsonDocument incoming(UDP_TX_PACKET_MAX_SIZE);
    deserializeJson(incoming, packetBuffer);
    if (incoming["mac"] == "B4:6D:83:CA:AF:C6") Serial.println("YES");
  }
}

void gestion_energie::ds18search() {
  nbDS18 = 0;
  JsonArray DS18 = doc["scan"].createNestedArray("ds18");
  while (ds18.search(sensorDS18[nbDS18].id)) {
    Serial.print("ROM = ");
    String id = "";
    for (uint8_t i = 0; i < 8; i++) {
      Serial.printf("%02X",sensorDS18[nbDS18].id[i]);
      String hex = String(sensorDS18[nbDS18].id[i], HEX);
      hex.toUpperCase();
      if (hex.length() == 1) id += "0";
      id += hex;
    }
    if (OneWire::crc8(sensorDS18[nbDS18].id, 7) != sensorDS18[nbDS18].id[7]) {
      Serial.println(" CRC is not valid!");
      //break;
    } else {
      Serial.println();
      DS18.add(id);
      nbDS18++;
    }
    if (nbDS18 > NBMAXDS18) break;
  }
  //Serial.printf("ds18search:%u founds\n",nbDS18);
  Serial.print("ds18 found : ");
  serializeJson(DS18, Serial);
  Serial.println();
}

void gestion_energie::I2Cscanner() {
  uint8_t nDevices = 0;
  JsonArray i2c = doc["scan"].createNestedArray("i2c");
  Serial.println("Scanning I2C...");
  for (uint8_t address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    uint8_t error = Wire.endTransmission();
    if (error == 0) {
      String hex = String(address, HEX);
      hex.toUpperCase();
      if (hex.length() == 1) hex = "0x0" + hex;
      else hex = "0x" + hex;
      i2c.add(hex);
      //Serial.printf("I2C device found at address 0x%02X\n",address);
      nDevices++;
    } else if (error == 4) {
      Serial.printf("Unknown error at address 0x%02X\n", address);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else {
    Serial.print("I2C found : ");
    serializeJson(i2c, Serial);
    Serial.println();
  }
}

void gestion_energie::screen_init() {
  Serial.println("screen_init()");
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 0, "BOOTING...");
  display.drawXbm((SSD1306_LCDWIDTH - WiFi_Logo_width) / 2,
                  (SSD1306_LCDHEIGHT - WiFi_Logo_height) / 2 + 15,
                  WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.setFont(ArialMT_Plain_10);
  display.display();
}

void gestion_energie::screen() {  //128x64 bi color
  display.clear();
  display.drawXbm(SSD1306_LCDWIDTH - logo_width, SSD1306_LCDHEIGHT - logo_height, logo_width, logo_height, logo_bits);
  if (WiFi.status() != WL_CONNECTED) {
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 0, "WIFI NA");
    display.setFont(ArialMT_Plain_10);
  } else {
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(SSD1306_LCDWIDTH - logo_width - 2, SSD1306_LCDHEIGHT - 10, this->date.c_str());
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawXbm(SSD1306_LCDWIDTH - logo_wifi_width, 0, logo_wifi_width, logo_wifi_height, logo_wifi_bits);
  }
  uint8_t LCD_Y = 15;
  static int8_t screenrot = 0;
  switch (screenrot) {
    case 0:
      {
        display.drawString(0, 0, "RESEAU");
        display.drawString(0, LCD_Y, WiFi.localIP().toString().c_str());
        display.drawString(0, LCD_Y += 10, WiFi.macAddress().c_str());
        display.drawString(0, LCD_Y += 10, WiFi.SSID().c_str());
        break;
      }
    case 1:
      {
        display.drawString(0, 0, "RELAYAGE");
        display.drawString(0, LCD_Y, "O31->O0");
        uint32_t n = this->outputs;
        LCD_Y += 10;
        for (uint8_t h=0;h<2;h++){
          for (uint8_t i = 0; i < 16; i++) {
            if (n & 0x80000000) display.drawString(i * 6, LCD_Y, "1");
            else display.drawString(i * 6, LCD_Y, "0");
            n <<= 1;
          }
          LCD_Y += 10;
        }
        break;
      }
    case 2:
      {
        static uint8_t page = 0;
        uint8_t totalPages = nbACS712/8;
        if (nbACS712%8 != 0) totalPages++;
        uint8_t j = 0;        
        String title = "INTENSITES ";
        if (nbACS712 >8){
          title += String(page+1) + "/" + String(totalPages);
          j = page*8;
        }
        display.drawString(0, 0, title);
        String s = "";
        for (uint8_t i=j ; i < _min(j+8,nbACS712); i++) {
          s += "i" + (String)i + ":";
          s += String(acs712[i].Irms, 1) + " ";
        }
        display.drawStringMaxWidth(0, LCD_Y, SSD1306_LCDWIDTH - logo_width, s);
        if (nbACS712 >8 && page<(totalPages-1)){
          page++;
          screenrot--;
        }
        else page=0;
        break;
      }
    case 3:
      {
        static uint8_t page = 0;
        uint8_t totalPages = nbDS18/8;
        if(nbDS18%8 != 0) totalPages++;
        uint8_t j = 0;        
        String title = "TEMPERATURES ";
        if (nbDS18 >8){
          title += String(page+1) + "/" + String(totalPages);
          j = page*8;
        }
        display.drawString(0, 0, title);
        String s = "";
        for (uint8_t i=j ; i < _min(j+8,nbDS18); i++) {
          s += "T" + (String)i + ":";
          s += (String)(int)sensorDS18[i].temp + " ";
        }
        display.drawStringMaxWidth(0, LCD_Y, SSD1306_LCDWIDTH - logo_width, s);
        if (nbDS18 >8 && page<(totalPages-1)){
          page++;
          screenrot--;
        }
        else{
          page=0;
          screenrot = -1;          
        } 
        break;
      }
    #if TELEINFO
    case 4:
      {
        display.drawString(0, 0, "TELEINFO");
        display.println(tinfo.valuesDump());
        break;
      }
    #endif
  }
  screenrot++;
  display.display();
  
}