/*
 * damien varrel 28-10-2022
 * if call ESP.restart();  GPIO0(D3) need to be HIGH and GPIO15(D8) =LOW
*/
#include "def.h"

gestion_energie ge;

void setup() 
{
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);
  delay(10);
  Serial.printf("sketch date %s %s\n",__DATE__,__TIME__);
  
  ge.screen_init();
  ge.wifi_init();
  ge.HTTPupdate();
  ge.gestion_init();
}



void loop() {
  static boolean start=true;
  static uint32_t dt_5s=0,dt_1min=millis(),dt_1h=millis();

  if (millis()-dt_5s>5*1000) {
    dt_5s=millis();
    digitalWrite(BUILTIN_LED,0);
    delay(10);
    digitalWrite(BUILTIN_LED,1);
    ge.screen();
    ge.HTTPgetsetOutputs();
    ge.HTTPsendvalues();
  }
  if((millis()-dt_1min>1000*60 || start) && WiFi.status()==WL_CONNECTED) {
    dt_1min=millis();
    Serial.println("dt_1min");
    Serial.printf("memory fragmentation:%.2f\n",getFragmentation());
    ge.ireads();
    ge.ds18read();
    #if TELEINFO
      ge.teleinfo();
    #endif
    ge.HTTPsendvalues();
    ge.HTTPmustReset();
  }
  if (millis()-dt_1h>1000*3600 || start) {
    dt_1h=millis();
    Serial.println("dt_1h");
    ge.HTTPupdate();
  }
  
  start=false;
}

#include <stddef.h> // for size_t
#include <Esp.h>

// Returns the number of free bytes in the RAM.
size_t getTotalAvailableMemory() {
  return ESP.getFreeHeap();
}
// Returns the size of the largest allocable block of RAM.
size_t getLargestAvailableBlock() {
  return ESP.getMaxFreeBlockSize();
}

// Computes the heap fragmentation percentage.
inline float getFragmentation() {
  return 100 - getLargestAvailableBlock() * 100.0 / getTotalAvailableMemory();
}

