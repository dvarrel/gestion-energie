#ifndef _def_H
#define _def_H

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <ESP8266httpUpdate.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <OneWire.h> 
#include <Adafruit_ADS1X15.h>
#include <PCF8574.h>
#include <ArduinoJson.h>
#include <SoftwareSerial.h>
#include <SSD1306Wire.h>
#include "images.h"

#define TELEINFO false
#if TELEINFO
#include <LibTeleinfo.h> //https://github.com/hallard/LibTeleinfo
#endif

#define PIN_CS2 //D0 IO  GPIO16
#define PIN_SCL D1 //D1  IO, SCL GPIO5
#define PIN_SDA D2 //D2  IO, SDA GPIO4
#define PIN_DS18 D3 //D3  IO, 10k Pull-up GPIO0
 //D4  IO, 10k Pull-up, BUILTIN_LED GPIO2

#define PIN_SCK D5 //D5  IO, SCK     GPIO14
#define PIN_MISO D6 //D6   IO, MISO   GPIO12
#define PIN_CS2 D7   //D7 IO, MOSI   GPIO13
#define PIN_TELEINFO D8 //D8  IO, 10k Pull-down, SS GPIO15

#define SSD1306_LCDWIDTH 128
#define SSD1306_LCDHEIGHT 64
#define SSD1306_ADDRESS 0x3C

#define NBMAXPCF8574 4
#define PCF_FIRST_ADDRESS 0x38 //prochaines adresses 0x39, 0x3A, 0x3B
#define NBMAXDS18 16
#define TEMPERROR 127
#define NBMAXADS1015 4
#define ADS_FIRST_ADDRESS 0x48 // 1001 000 (ADDR -> GND)
//prochaines adresses 0x49 (ADDR -> Vcc),0x4A(ADDR -> sda),0x4B(ADDR -> scl)

#define NBMAXACS712 NBMAXADS1015*4
#define ACS712_SENS5A 185
#define ACS712_SENS20A 100
#define ACS712_SENS30A 66

#define UDP_LOCAL_PORT  12345// local port to listen for UDP packets
#define UDP_REMOTE_PORT 12345// remote port to send UDP packets

class gestion_energie {
  private:
    
    OneWire  ds18;
    struct _sensorDS18 {uint8_t id[8];float temp;} sensorDS18[NBMAXDS18];
    uint8_t nbDS18=0;

    Adafruit_ADS1015 ads1015[NBMAXADS1015] = {Adafruit_ADS1015(),Adafruit_ADS1015(),Adafruit_ADS1015(),Adafruit_ADS1015()};
    uint8_t nbADS1015=0;
    
    PCF8574 pcf8574[NBMAXPCF8574] = {PCF8574(),PCF8574(),PCF8574(),PCF8574()};
    uint8_t nbPCF8574=0;

    uint32_t outputs=0x0000,last_outputs=0x0000;

    struct _acs712 { uint8_t sensitivity;float Urms; float Irms;} acs712[NBMAXACS712];
    uint8_t nbACS712 = 0;

    WiFiClient client;
    const char *CALENDAR="https://www.varrel.fr/gestion-energie/CALENDAR.php";
    const char *URLPOST= "https://www.varrel.fr/gestion-energie/PostJSON.php";
    const char *URLUPDATE = "https://www.varrel.fr/gestion-energie/update/update.php";
    WiFiUDP Udp; // A UDP instance to let us send and receive packets over UDP
    String MAC, date;
    IPAddress broadcast;
    StaticJsonDocument<2048> doc;
    StaticJsonDocument<2048> reponse;
    #if TELEINFO
      #define SW_SERIAL_UNUSED_PIN -1
      SoftwareSerial myserial(PIN_TELEINFO,SW_SERIAL_UNUSED_PIN); // Teleinfo Serial
      TInfo tinfo; // Teleinfo object
      String teleinfoData = "";
    #endif
    
  public:
    gestion_energie(){
      ds18.begin(PIN_DS18);  // 10K to Vdd exist on board (DS18B20 needs 4.7K to Vdd)
    }
    void wifi_init();
    void gestion_init();

    void HTTPgetsetOutputs();
    void HTTPpost(const char* url, String& data); 
    void HTTPsendvalues();
    void HTTPupdate();
    void HTTPmustReset();
    void UDPsendValues();
    void UDPsendRequest();

    void relays(uint32_t nums);
    void ds18read();
    void ds18search();
    void ireads();
    void I2Cscanner();
    #if TELEINFO
    void teleinfoJSON(ValueList * me);
    void teleinfo(uint16_t delayms=2000);
    #endif
    
    void screen_init();
    void screen();
};

#endif
